#include "Board.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Rook.h"
#include "Queen.h"
#include "Characters.h"
#include<iostream>


//constructor
Board::Board(std::string startPos)
{
    int onString = 0;
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            switch (startPos[onString])
            {

                case WHITE_ROOK:
                    this->_board[i][j] = new Rook("R", WHITE);
                    break;
                case WHITE_PAWN:
                    this->_board[i][j] = new Pawn("P", WHITE);
                    break;
                case WHITE_KING:
                    this->_board[i][j] = new King("K", WHITE);
                    break;
                case WHITE_KNIGHT:
                    this->_board[i][j] = new Knight("N", WHITE);
                    break;
                case WHITE_BISHOP:
                    this->_board[i][j] = new Bishop("B", WHITE);
                    break;
                case WHITE_QUEEN:
                    this->_board[i][j] = new Queen("Q", WHITE);
                    break;
                //BLACK CHARACTERS// 

                case BLACK_KNIGHT:
                    this->_board[i][j] = new Knight("n", BLACK);
                    break;
                case BLACK_BISHOP:
                    this->_board[i][j] = new Bishop("b", BLACK);
                    break;
                case BLACK_KING:
                    this->_board[i][j] = new King("k", BLACK);
                    break;
                case BLACK_QUEEN:
                    this->_board[i][j] = new Queen("q", BLACK);
                    break;
                case BLACK_ROOK:
                    this->_board[i][j] = new Rook("r", BLACK);
                    break;
                case BLACK_PAWN:
                    this->_board[i][j] = new Pawn("p", BLACK);
                    break;
                default:
                    this->_board[i][j] = nullptr;
                    break;
            }
            onString++;
        }
    }
        
}

//copy constructor
Board::Board(Board& other)
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            if (other._board[i][j] == nullptr)
            {
                this->_board[i][j] = nullptr;
            }
            else
            {
                switch (other._board[i][j]->getName()[0])
                {
                case WHITE_ROOK:
                    this->_board[i][j] = new Rook("R", WHITE);
                    break;
                case WHITE_PAWN:
                    this->_board[i][j] = new Pawn("P", WHITE);
                    break;
                case WHITE_KING:
                    this->_board[i][j] = new King("K", WHITE);
                    break;
                case WHITE_KNIGHT:
                    this->_board[i][j] = new Knight("N", WHITE);
                    break;
                case WHITE_BISHOP:
                    this->_board[i][j] = new Bishop("B", WHITE);
                    break;
                case WHITE_QUEEN:
                    this->_board[i][j] = new Queen("Q", WHITE);
                    break;
                    //BLACK CHARACTERS// 

                case BLACK_KNIGHT:
                    this->_board[i][j] = new Knight("n", BLACK);
                    break;
                case BLACK_BISHOP:
                    this->_board[i][j] = new Bishop("b", BLACK);
                    break;
                case BLACK_KING:
                    this->_board[i][j] = new King("k", BLACK);
                    break;
                case BLACK_QUEEN:
                    this->_board[i][j] = new Queen("q", BLACK);
                    break;
                case BLACK_ROOK:
                    this->_board[i][j] = new Rook("r", BLACK);
                    break;
                case BLACK_PAWN:
                    this->_board[i][j] = new Pawn("p", BLACK);
                    break;
                }
            }
        }
    }
}

//destructor
Board::~Board()
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            if(this->_board[i][j] != NULL)
            {
                delete this->_board[i][j];
            }
        }
    }
}

// method to check if there is check on the board
// gets the king color
//outputs 1 if the given player is under check. 0 if not
bool Board::isCheck(const bool kingColor)
{
    int kingCol = 0;
    int kingRow = 0;
    int i = 0;
    int j = 0;
    for (i = 0; i < SIZE; i++)
    {
        for (j = 0; j < SIZE; j++)
        {
            if (this->_board[i][j] != nullptr) // finding the king 
            {
                if (kingColor == WHITE && this->_board[i][j]->getName() == "K")
                {
                    kingCol = i;
                    kingRow = j;
                    break;
                }
                else if (kingColor == BLACK && this->_board[i][j]->getName() == "k")
                {
                    kingCol = i;
                    kingRow = j;
                    break;
                }
            }
        }
        if (kingCol == i && kingRow == j)
        {
            break;
        }
    }
    // trying every possible move that captures the king on the board to see if there is 1 available
    for (i = 0; i < SIZE; i++) 
    {
        for (j = 0; j < SIZE; j++)
        {
            if (this->_board[i][j] != nullptr)
            {
                if (this->_board[i][j]->getColor() != kingColor)
                {
                    if (this->_board[i][j]->move(i, j, kingCol, kingRow, this->_board))
                    {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

// BONUS //
/*The method will check if there is checkmate on the board
input: the color of the king that might have gotten mated
output: true if there is mate on the board false otherwise*/
bool Board::isMate(const bool kingColor)
{
    if (!this->isCheck(kingColor))
    {
        return 0;
    }
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++) // running over every piece
        {
            if (this->_board[i][j] != nullptr && this->_board[i][j]->getColor() == kingColor)
            {
                for (int k = 0; k < SIZE; k++)
                {
                    for (int l = 0; l < SIZE; l++) // for every piece running over every possible move
                    {
                        if (this->_board[i][j]->move(i, j, k, l, this->_board))
                        {
                            Board tempBoard(*this); // playing every move on a temp board
                            if (tempBoard._board[k][l] != nullptr)
                            {
                                delete tempBoard._board[k][l];
                            }
                            tempBoard._board[k][l] = tempBoard._board[i][j];
                            tempBoard._board[i][j] = nullptr;
                            if (!tempBoard.isCheck(kingColor)) //if the move that was played got the king out of check there is no checkmate
                            {
                                return false;
                            }

                        }
                    }
                }
            }
        }
    }
    return true;
}

// method to print a board
void Board::printBoard()
{
    for(int i = 0; i<SIZE;i++) 
    {
        for(int j = 0; j < SIZE; j++)
        {
            if (this->_board[i][j] == NULL)
            {
                std::cout << "#" << " ";
            }
            else
            {
                std::cout << this->_board[i][j]->getName() << " ";
            }
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}
/*
* This function gets the code of a move
* input: index of the move
* output: the move's code. 0 - valid, 1 - valid + check etc...
*/
char Board::makeMove(int startCol, int startRow, int endCol, int endRow, bool player)
{
    bool isValid = 0;
    //if given move is out of the board index
    if (startCol > 7 || startCol < 0 || startRow > 7 || startRow < 0 || endRow > 7 || endRow < 0 || endCol > 7 || endCol < 0)
    {
        return '5';
    }
    if (!this->_board[startCol][startRow]) //if the player is trying to move a piece in an empty spot
    {
        return '2';
    }
    if (this->_board[startCol][startRow]->getColor() != player)
    {
        return '2'; // if the player is trying to move the enemy's piece
    }
    if (startCol == endCol && startRow == endRow)
    {
        return '7'; // if both starting point and ending point of the move are the same
    }
    if (this->_board[endCol][endRow] != nullptr)
    {
        if (this->_board[startCol][startRow]->getColor() == this->_board[endCol][endRow]->getColor())
        {
            return '3'; // if the player is trying to capture it's own piece
        }
    }

    isValid = this->_board[startCol][startRow]->move(startCol, startRow, endCol, endRow, this->_board);
    if (!isValid)
    {
        return '6'; //illegal move 
    }
    if (isValid)
    {
        Board nextMove(*this);
        if (nextMove._board[endCol][endRow] != nullptr)
        {
            delete nextMove._board[endCol][endRow];
        }
        nextMove._board[endCol][endRow] = nextMove._board[startCol][startRow];
        nextMove._board[startCol][startRow] = nullptr;
        if (nextMove.isCheck(player))
        {
            if (this->_board[startCol][startRow]->getName() == "P" || this->_board[startCol][startRow]->getName() == "p")
            {
                ((Pawn*)(this->_board[startCol][startRow]))->setDidMove(0);
            }
            return '4'; //moving into check
        }

        if (this->_board[endCol][endRow] != nullptr)
        {
            delete this->_board[endCol][endRow];
        }
        this->_board[endCol][endRow] = this->_board[startCol][startRow];
        this->_board[startCol][startRow] = nullptr;
        if (this->isMate(!player))
        {
            return '8'; // valid move it's checkmate!
        }
        if (this->isCheck(!player))
        {
            return '1'; // valid move created check
        }
        return '0'; // valid move
    }
}

//static method to turn chess board notation into 2D array indexes
int Board::boardToIndex(char index)
{
    if ((int)index >= 97)
    {
        return (int)index - 97;
    }
    switch (index) // lining chess notation up with 2D array indexes
    {
        case '8':
            return 0;
        case '7':
            return 1;
        case '6':
            return 2;
        case '5':
            return 3;
        case '4':
            return 4;
        case '3':
            return 5;
        case '2':
            return 6;
        case '1':
            return 7;
    }
}
