#include "Pawn.h"

//constructor
Pawn::Pawn(std::string name, int _color) : Piece(name, _color)
{
	this->_didMove = false;
}

//getter 
bool Pawn::getDidMove() const
{
	return this->_didMove;
}

//setter
void Pawn::setDidMove(const bool set)
{
	this->_didMove = set;
}

/*The pawn's move
 * gets move indexes
 * returns true if the move is valid
*/
bool Pawn::move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE])
{
	if (board[endCol][endRow] != nullptr)
	{
		if (board[startCol][startRow]->getColor() == board[endCol][endRow]->getColor())
		{
			return false;
		}
	}
	//normal move
	if (endCol == startCol + 1 && endRow == startRow && board[startCol][startRow]->getColor() == BLACK)
	{
		if (startRow == endRow && board[endCol][endRow] == nullptr)
		{
			this->_didMove = 1;
			return true;
		}
	}
	if (endCol == startCol - 1 && endRow == startRow && board[startCol][startRow]->getColor() == WHITE)
	{
		if (startRow == endRow && board[endCol][endRow] == nullptr)
		{
			this->_didMove = 1;
			return true;
		}

	}
	//jump 2 if it's the first move
	if (endCol == startCol + 2 && endRow == startRow && board[startCol][startRow]->getColor() == BLACK && this->_didMove == 0)
	{
		if (startRow == endRow && board[endCol][endRow] == nullptr)
		{
			this->_didMove = 1;
			return true;
		}
	}
	else if(endCol == startCol - 2 && endRow == startRow && board[startCol][startRow]->getColor() == WHITE && this->_didMove == 0)
	{
		if (startRow == endRow && board[endCol][endRow] == nullptr)
		{
			this->_didMove = 1;
			return true;
		}
	}
	//capture a piece 
	if(this->getColor() == BLACK && endCol == startCol + 1)
	{
		if (abs(endRow - startRow) == 1)
		{
			if (board[endCol][endRow] != nullptr)
			{

				if (board[endCol][endRow]->getColor() != this->getColor())
				{
					this->_didMove = 1;
					return true;
				}
			}
		}
	}
	if (this->getColor() == WHITE && endCol == startCol - 1)
	{
		if (abs(endRow - startRow) == 1)
		{
			if (board[endCol][endRow] != nullptr)
			{

				if (board[endCol][endRow]->getColor() != this->getColor())
				{
					this->_didMove = 1;
					return true;
				}
			}
		}
	}
	return false;
}


