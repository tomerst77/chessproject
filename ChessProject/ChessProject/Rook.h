#pragma once

#ifndef ROOK_H
#define ROOK_H

#include"Piece.h"

class Rook : public Piece
{
public:
	Rook(std::string name, int _color);
	~Rook() = default;

	bool move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE]) override;
};



#endif

