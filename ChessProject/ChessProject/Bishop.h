#pragma once
#ifndef BISHOP_H
#define BISHOP_H

#include"Piece.h"

class Bishop : public Piece
{
public:
	Bishop(std::string name, int _color);
	~Bishop() = default;

	bool move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE]) override;

};


#endif
