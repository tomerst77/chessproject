#pragma once

#ifndef QUEEN_H
#define QUEEN_H

#include"Piece.h"

class Queen : public Piece
{
public:
	Queen(std::string name, int _color);
	~Queen() = default;

	bool move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE]) override;
};

#endif
