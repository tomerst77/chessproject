/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/
#include "Pipe.h"
#include <iostream>
#include <thread>
#include"Board.h"
#include"Characters.h"

using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time(NULL));
	bool player = (rand() % 2); // decide who starts randomly
	char code[2] = " ";
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0");
	msgToGraphics[64] = (char)player + 48;
	Board game(msgToGraphics); // create board

	p.sendMessageToGraphics(msgToGraphics); // send frontend the board
	string msgFromGraphics = "";

	while (msgFromGraphics != "quit") // game loop
	{
		game.printBoard();
		msgFromGraphics = p.getMessageFromGraphics(); // get the move the player made
		
		code[0] = game.makeMove(Board::boardToIndex(msgFromGraphics[1]), Board::boardToIndex(msgFromGraphics[0]), Board::boardToIndex(msgFromGraphics[3]), Board::boardToIndex(msgFromGraphics[2]), player);


		strcpy_s(msgToGraphics, code); //copy the move's code into a buffer
		
		p.sendMessageToGraphics(msgToGraphics); // send move code
		if (code[0] == '8')
		{
			std::cout << "CHECKMATE!" << endl;
		}
		if (code[0] == '0' || code[0] == '1') // change turn
		{
			player = !player;
		}
	}

	p.close();
}