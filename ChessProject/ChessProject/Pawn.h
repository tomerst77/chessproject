#pragma once
#ifndef PAWN_H
#define PAWN_H

#include"Piece.h"

class Pawn : public Piece
{
	bool _didMove;
public:
	Pawn(std::string name, int _color);
	~Pawn() = default;

	bool getDidMove() const;
	void setDidMove(const bool set);
	bool move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE]) override;
};

#endif

