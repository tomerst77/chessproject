#include "King.h"

//constructor
King::King(std::string name, int _color) : Piece(name, _color)
{
}

/*King move
input: move indexes
output: 1 if the move is valid. 0 otherwise*/
bool King::move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE])
{
	if (board[endCol][endRow] != nullptr)
	{
		if (board[startCol][startRow]->getColor() == board[endCol][endRow]->getColor())
		{
			return false;
		}
	}
	if (endCol != startCol + 1 && endCol != startCol - 1 && endCol != startCol)
	{
		return false;
	}
	if (endRow != startRow + 1 && endRow != startRow - 1 && endRow != startRow)
	{
		return false;
	}
	return true;
}

