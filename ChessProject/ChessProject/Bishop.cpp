#include "Bishop.h"

//constructor
Bishop::Bishop(std::string name, int _color) : Piece(name, _color)
{
}

// bishop move 
//input move indexes
//output: 1 if the move is valid, 0 otherwise
bool Bishop::move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE])
{
	if (board[endCol][endRow] != nullptr)
	{
		if (board[startCol][startRow]->getColor() == board[endCol][endRow]->getColor())
		{
			return false;
		}
	}
	if (startCol == endCol || startRow == endRow)
	{
		return false;
	}
	if ((abs(startCol - endCol)) == (abs(startRow - endRow)))
	{
		//checking if there is anything in the bishop's way
		if (startCol > endCol && startRow > endRow)
		{
			for (int i = 1; i < startCol - endCol; i++)
			{
				if (board[startCol - i][startRow - i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}
		if (startCol < endCol && startRow < endRow)
		{
			for (int i = 1; i < endCol - startCol; i++)
			{
				if (board[startCol + i][startRow + i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}
		if (startCol < endCol && startRow > endRow)
		{
			for (int i = 1; i < endCol - startCol; i++)
			{
				if (board[startCol + i][startRow - i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}
		if (startCol > endCol && startRow < endRow)
		{
			for (int i = 1; i < endRow - startRow; i++)
			{
				if (board[startCol - i][startRow + i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}

			
	}
	return false;
}
