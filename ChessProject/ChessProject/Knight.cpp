#include "Knight.h"

//constructor
Knight::Knight(std::string name, int _color) : Piece(name, _color)
{
}

//knight move method
//input: move indexes
//output: 1 if the move is valid. 0 otherwise
bool Knight::move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE])
{
	if (board[endCol][endRow] != nullptr)
	{
		if (board[startCol][startRow]->getColor() == board[endCol][endRow]->getColor())
		{
			return false;
		}
	}
	if (startCol == endCol || startRow == endRow)
	{
		return false;
	}
	if (endCol == startCol + 2 && endRow == startRow + 1 || endCol == startCol + 2 && endRow == startRow - 1)
	{
		return true;
	}
	if (endCol == startCol - 2 && endRow == startRow + 1 || endCol == startCol - 2 && endRow == startRow - 1)
	{
		return true;
	}
	if (endRow == startRow + 2 && endCol == startCol + 1 || endRow == startRow + 2 && endCol == startCol - 1)
	{
		return true;
	}
	if (endRow == startRow - 2 && endCol == startCol + 1 || endRow == startRow - 2 && endCol == startCol - 1)
	{
		return true;
	}

	return false;
}
