#include "Queen.h"

//constructor
Queen::Queen(std::string name, int _color) : Piece(name, _color)
{
}

//queen move method (combines the rook and bishop)
//input: move indexes
//output: 1 if the move is valid. 0 otherwise
bool Queen::move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE])
{
	if (board[endCol][endRow] != nullptr)
	{
		if (board[startCol][startRow]->getColor() == board[endCol][endRow]->getColor())
		{
			return false;
		}
	}
	//bishop part
	if ((abs(startCol - endCol)) == (abs(startRow - endRow)))
	{
		if (startCol > endCol && startRow > endRow)
		{
			for (int i = 1; i < startCol - endCol; i++)
			{
				if (board[startCol - i][startRow - i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}
		if (startCol < endCol && startRow < endRow)
		{
			for (int i = 1; i < endCol - startCol; i++)
			{
				if (board[startCol + i][startRow + i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}
		if (startCol < endCol && startRow > endRow)
		{
			for (int i = 1; i < endCol - startCol; i++)
			{
				if (board[startCol + i][startRow - i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}
		if (startCol > endCol && startRow < endRow)
		{
			for (int i = 1; i < endRow - startRow; i++)
			{
				if (board[startCol - i][startRow + i] != nullptr)
				{
					return false;
				}
			}
			return true;
		}
	}
	//rook part
	if ((startCol != endCol && startRow == endRow) || (startRow != endRow && startCol == endCol))
	{
		if (startRow != endRow)
		{
			for (int i = fmin(startRow, endRow) + 1; i < fmax(startRow, endRow); i++)
			{
				if (board[startCol][i] != nullptr)
				{
					return false;
				}

			}
			return true;
		}
		else if (startCol != endCol)
		{
			for (int i = fmin(startCol, endCol) + 1; i < fmax(startCol, endCol); i++)
			{
				if (board[i][startRow] != nullptr)
				{
					return false;
				}
			}
			return true;

		}
	}
	return false;

}
