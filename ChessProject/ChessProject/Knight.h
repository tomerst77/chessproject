#pragma once
#ifndef KNIGHT_H
#define KNIGHT_H

#include "Piece.h"

class Knight : public Piece
{
public:
    Knight(std::string name, int _color);
    ~Knight() = default;

    bool move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE]) override;
};

#endif
