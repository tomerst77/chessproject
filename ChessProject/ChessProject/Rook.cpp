#include "Rook.h"

//rook constructor
Rook::Rook(std::string name, int _color) : Piece(name, _color)
{
}

// method to check if a rook move is valid
// input: move indexes and board
// output: 1 if valid. 0 otherwise
bool Rook::move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE])
{
	if (board[endCol][endRow] != nullptr)
	{
		if (board[startCol][startRow]->getColor() == board[endCol][endRow]->getColor()) 
		{
			return false;
		}
	}
	if(startCol != endCol && startRow != endRow)
	{
		return false;
	}
	if ((startCol != endCol && startRow == endRow) || (startRow != endRow && startCol == endCol))
	{
		if (startRow != endRow)
		{
			for (int i = fmin(startRow, endRow) + 1; i < fmax(startRow, endRow); i++)
			{
				if (board[startCol][i] != nullptr)
				{
					return false;
				}

			}
			return true;
		}
		else if (startCol != endCol)
		{
			for (int i = fmin(startCol, endCol) + 1; i < fmax(startCol, endCol); i++)
			{
				if (board[i][startRow] != nullptr)
				{
					return false;
				}
				

			}
			return true;
		}
	}
	return false;

}