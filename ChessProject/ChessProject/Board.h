#pragma once
#ifndef BOARD_H
#define BOARD_H

#include "Piece.h"
#include <string>
#define SIZE 8
class Piece;
class Board
{
private:
	Piece* _board[SIZE][SIZE];

public:
	Board(std::string startPos);
	Board(Board &other);
	~Board();

	bool isCheck(const bool kingColor);
	bool isMate(const bool kingColor);
	void printBoard();
    char makeMove(int startCol, int startRow, int endCol, int endRow, bool player);
	static int boardToIndex(char index);
};

#endif 

