#include "Piece.h"

//getter for color
int Piece::getColor() const
{
	return this->_color;
}

//getter for name
std::string Piece::getName() const
{
	return this->_name;
}

// constructor
Piece::Piece(std::string name, int color)
{
	this->_name = name;
	this->_color = color;
}
