#pragma once
#ifndef KING_H
#define KING_H


#include "Piece.h"
class King : public Piece
{
public:
    King(std::string name, int _color);
    ~King() = default;

    bool move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE]) override;
};

#endif

