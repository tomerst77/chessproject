#pragma once
#ifndef PIECE_H
#define PIECE_H

#include <string>
#include "Board.h"
#include "Characters.h"

#define SIZE 8

class Board;


class Piece
{

protected:
    int _color;
    std::string _name;
public:

    int getColor() const;
    std::string getName() const;

    /*
    * bool method to check if a move is valid
    * input: move indexes(start point and end point) and a board
    * output: 1 if the move is valid, 0 if not
    */
    virtual bool move(int startCol, int startRow, int endCol, int endRow, Piece* board[SIZE][SIZE]) = 0;
    Piece(std::string name, int color);
    ~Piece() = default;
};


#endif
